package com.chuongvd.socketmanager.server;

public class ServerSocketManager {
    public static final int SOCKET_SERVER_PORT = 8080;

    private String message = "";

    private static ServerSocketManager sInstance;

    public static ServerSocketManager getInstance() {
        if (sInstance == null) {
            sInstance = new ServerSocketManager();
        }
        return sInstance;
    }

    private ServerSocketManager() {
    }

    public int getPort() {
        return SOCKET_SERVER_PORT;
    }

    public ServerSocketThread initServer() {
        return ServerSocketThread.getsInstance();
    }

    public void onDestroy() {
        ServerSocketThread.getsInstance().interrupt();
    }

    //    private class SocketServerReplyThread extends Thread {
    //
    //        private Socket hostThreadSocket;
    //        int cnt;
    //
    //        SocketServerReplyThread(Socket socket, int c) {
    //            hostThreadSocket = socket;
    //            cnt = c;
    //        }
    //
    //        @Override
    //        public void run() {
    //            OutputStream outputStream;
    //            try {
    //                String msgReply = "Hello from Server, you are #" + cnt;
    //                outputStream = hostThreadSocket.getOutputStream();
    //                PrintStream printStream = new PrintStream(outputStream);
    //                printStream.print(msgReply);
    //                //                printStream.close();
    //
    //                message += "replayed: " + msgReply + "\n";
    //            } catch (IOException e) {
    //                // TODO Auto-generated catch block
    //                e.printStackTrace();
    //                message += "Something wrong! " + e.toString() + "\n";
    //            }
    //            try {
    //                sleep(3000);
    //            } catch (InterruptedException e) {
    //                e.printStackTrace();
    //            }
    //        }
    //    }
    //
    //    public String getIpAddress() {
    //        String ip = "";
    //        try {
    //            Enumeration<NetworkInterface> enumNetworkInterfaces =
    //                    NetworkInterface.getNetworkInterfaces();
    //            while (enumNetworkInterfaces.hasMoreElements()) {
    //                NetworkInterface networkInterface = enumNetworkInterfaces.nextElement();
    //                Enumeration<InetAddress> enumInetAddress = networkInterface.getInetAddresses();
    //                while (enumInetAddress.hasMoreElements()) {
    //                    InetAddress inetAddress = enumInetAddress.nextElement();
    //
    //                    if (inetAddress.isSiteLocalAddress()) {
    //                        ip += "Server running at : " + inetAddress.getHostAddress();
    //                    }
    //                }
    //            }
    //        } catch (SocketException e) {
    //            // TODO Auto-generated catch block
    //            e.printStackTrace();
    //            ip += "Something Wrong! " + e.toString() + "\n";
    //        }
    //        return ip;
    //    }
}

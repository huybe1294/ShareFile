package com.chuongvd.socketmanager.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerSocketThread extends Thread {

    private ServerSocket mServerSocket;

    private static ServerSocketThread sInstance;

    public static ServerSocketThread getsInstance() {
        if (sInstance == null) {
            sInstance = new ServerSocketThread();
        }
        if (sInstance.isInterrupted()) {
            sInstance = new ServerSocketThread();
        }
        return sInstance;
    }

    private ServerSocketThread() {
    }

    @Override
    public void run() {
        try {
            mServerSocket = new ServerSocket(ServerSocketManager.SOCKET_SERVER_PORT);
            while (!interrupted()) {
                Socket request = mServerSocket.accept();
                handlerRequest(request);
            }
            mServerSocket.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void handlerRequest(Socket request) {
        // TODO: 7/16/18
    }
}

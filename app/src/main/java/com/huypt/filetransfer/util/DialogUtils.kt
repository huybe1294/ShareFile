package com.huypt.filetransfer.util

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.support.v7.app.AlertDialog

class DialogUtils {
  companion object {
    fun showActionDialog(context: Context, title: String, message: String, positiveTitle: String,
        yesListener: DialogInterface.OnClickListener, negativeTitle: String,
        negativeListener: DialogInterface.OnClickListener, cancelable: Boolean): Dialog {
      return AlertDialog.Builder(context).setTitle(title)
          .setMessage(message)
          .setPositiveButton(positiveTitle, yesListener)
          .setNegativeButton(negativeTitle, negativeListener)
          .setCancelable(cancelable)
          .show()
    }
  }
}
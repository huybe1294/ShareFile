package com.huypt.filetransfer.data.db

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import com.huypt.filetransfer.data.DeviceInfo

@Dao
abstract class DeviceDao {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    abstract fun insert(deviceInfo: DeviceInfo)

    @Query("SELECT * FROM deviceinfo")
    abstract fun loadDevices(): LiveData<List<DeviceInfo>>
}
package com.huypt.filetransfer.data

import android.graphics.drawable.Drawable

class AppItem(id: String, title: String, val drawable: Drawable, uri: String,
    size: String) : TransferItem(id, title, uri, size, TransferItem.TYPE_APP)
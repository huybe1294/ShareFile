package com.huypt.filetransfer.data

import android.arch.persistence.room.Entity
import android.os.Build
import com.google.gson.Gson
import com.google.gson.annotations.SerializedName

@Entity(primaryKeys = ["ip"])
class DeviceInfo(val name: String = Build.MODEL, @field:SerializedName(
    "os_version") val osVersion: String = Build.VERSION.RELEASE,
    val playerName: String = Build.MANUFACTURER, val ip: String, val port: Int) {

  override fun toString(): String {
    return Gson().toJson(this)
  }

  companion object {
    fun fromJSON(jsonStr: String): DeviceInfo {
      val deviceInfo: DeviceInfo = Gson().fromJson(jsonStr, DeviceInfo::class.java)
      return deviceInfo
    }
  }
}
package com.huypt.filetransfer.data

import android.net.Uri

class VideoItem(id: String, title: String, uri: String, val thumb: Uri,
    size: String) : TransferItem(id = id,
    title = title, uri = uri, size = size, type = TransferItem.TYPE_VIDEO)
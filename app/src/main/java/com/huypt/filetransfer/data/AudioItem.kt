package com.huypt.filetransfer.data


class AudioItem(id: String, title: String, uri: String, size: String) : TransferItem(id = id,
    title = title, uri = uri, size = size, type = TransferItem.TYPE_AUDIO)
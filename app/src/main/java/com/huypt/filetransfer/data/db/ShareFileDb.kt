package com.huypt.filetransfer.data.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.huypt.filetransfer.data.DeviceInfo


@Database(entities = [DeviceInfo::class], version = 3, exportSchema = false)
abstract class ShareFileDb : RoomDatabase() {
    abstract fun deviceDao(): DeviceDao
}
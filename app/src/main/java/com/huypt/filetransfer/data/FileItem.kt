package com.huypt.filetransfer.data

 class FileItem(id: String, title: String, uri: String, size: String) : TransferItem(id = id,
         title = title, uri = uri, size = size, type = TransferItem.TYPE_FILE)
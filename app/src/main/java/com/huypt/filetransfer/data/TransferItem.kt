package com.huypt.filetransfer.data

open class TransferItem(var id: String, var title: String, var uri: String, var size: String, val type: String) {
  companion object {
    const val TYPE_FILE = "File"
    const val TYPE_APP = "App"
    const val TYPE_AUDIO = "Audio"
    const val TYPE_VIDEO = "Video"
    const val TYPE_PHOTO = "Photo"
  }
  var isSelected: Boolean = false
}
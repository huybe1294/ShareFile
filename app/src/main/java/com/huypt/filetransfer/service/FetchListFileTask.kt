package com.huypt.filetransfer.service

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.ContentResolver
import android.database.Cursor
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import com.huypt.filetransfer.data.*
import com.huypt.filetransfer.data.Resource
import com.huypt.filetransfer.util.FileUtils
import java.io.File
import java.util.*


@Suppress("UNCHECKED_CAST")
class FetchListFileTask<K> constructor(
        private val contentResolver: ContentResolver, private val type: String) : Runnable {

    private val _liveData = MutableLiveData<Resource<K>>()
    val liveData: LiveData<Resource<K>> = _liveData

    init {
        _liveData.postValue(Resource.loading(null))
    }
    override fun run() {
        fetchFileType(type)
    }

    private fun fetchFileType(type: String) {
        when (type) {
            TransferItem.TYPE_AUDIO -> {
                postAudioData()
            }
            TransferItem.TYPE_VIDEO -> {
                postVideoData()
            }
            TransferItem.TYPE_PHOTO -> {
                postPhotoData()
            }
            TransferItem.TYPE_FILE -> {
                postFileData(File(Environment.getExternalStorageDirectory().absolutePath))
            }
        }
    }

    private fun postAudioData() {
        val listFile: MutableList<AudioItem> = mutableListOf()
        val cursor = queriesAudio(contentResolver)
        if (cursor.count <= 0) {
            _liveData.postValue(Resource.success(null))
            return
        }
        cursor.moveToFirst()
        val idIndex = cursor.getColumnIndex(MediaStore.Audio.Media._ID)
        val titleIndex = cursor.getColumnIndex(MediaStore.Audio.Media.TITLE)
        val artistIndex = cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST)
        val albumIndex = cursor.getColumnIndex(MediaStore.Audio.Media.ALBUM)
        val durationIndex = cursor.getColumnIndex(MediaStore.Audio.Media.DURATION)
        val dataIndex = cursor.getColumnIndex(MediaStore.Audio.Media.DATA)
        val sizeIndex = cursor.getColumnIndex(MediaStore.Audio.Media.SIZE)
        do {
            val id = cursor.getString(idIndex)
            val title = cursor.getString(titleIndex)
            val uri = Uri.parse(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI.toString() + "/" + id).toString()
            val size = FileUtils.convertFileSize(cursor.getLong(sizeIndex), true)
            listFile.add(AudioItem(id, title, uri, size))
        } while (cursor.moveToNext())
        _liveData.postValue(Resource.success(listFile as K))
    }

    private fun postVideoData() {
        val listFile: MutableList<VideoItem> = mutableListOf()
        val cursor = queriesVideo(contentResolver)
        if (cursor.count <= 0) {
            _liveData.postValue(Resource.loading(null))
            return
        }
        cursor.moveToFirst()
        val idIndex = cursor.getColumnIndex(MediaStore.Video.Media._ID)
        val titleIndex = cursor.getColumnIndex(MediaStore.Video.Media.TITLE)
        val displayIndex = cursor.getColumnIndex(MediaStore.Video.Media.DISPLAY_NAME)
        val albumIndex = cursor.getColumnIndex(MediaStore.Video.Media.BUCKET_DISPLAY_NAME)
        val lengthIndex = cursor.getColumnIndex(MediaStore.Video.Media.DURATION)
        val dateIndex = cursor.getColumnIndex(MediaStore.Video.Media.DATE_MODIFIED)
        val sizeIndex = cursor.getColumnIndex(MediaStore.Video.Media.SIZE)
        val typeIndex = cursor.getColumnIndex(MediaStore.Video.Media.MIME_TYPE)
        do {
            val id = cursor.getString(idIndex)
            val title = cursor.getString(titleIndex)
            val display = cursor.getString(displayIndex)
            val album = cursor.getString(albumIndex)
            val length = cursor.getString(lengthIndex)
            val date = cursor.getString(dateIndex)
            val size = FileUtils.convertFileSize(cursor.getLong(sizeIndex), true)
            val type = cursor.getString(typeIndex)
            listFile.add(VideoItem(id, display, id,
                    Uri.parse(MediaStore.Video.Media.EXTERNAL_CONTENT_URI.toString() + "/" + id), size))
        } while (cursor.moveToNext())
        _liveData.postValue(Resource.success(listFile as K))
        Log.e("FetchListFileTaskk", Calendar.getInstance().time.toString())
    }

    private fun postPhotoData() {
        val listFile: MutableList<PhotoItem> = mutableListOf()
        val cursor = queriesPhoto(contentResolver)
        if (cursor.count <= 0) {
            _liveData.postValue(Resource.loading(null))
            return
        }
        cursor.moveToFirst()
        val idIndex = cursor.getColumnIndex(MediaStore.Images.Media._ID)
        val titleIndex = cursor.getColumnIndex(MediaStore.Images.Media.TITLE)
        val displayIndex = cursor.getColumnIndex(MediaStore.Images.Media.DISPLAY_NAME)
        val albumIndex = cursor.getColumnIndex(MediaStore.Images.Media.BUCKET_DISPLAY_NAME)
        val dataIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATA)
        val dateAddedIndex = cursor.getColumnIndex(MediaStore.Images.Media.DATE_MODIFIED)
        val sizeIndex = cursor.getColumnIndex(MediaStore.Images.Media.SIZE)
        val typeIndex = cursor.getColumnIndex(MediaStore.Images.Media.MIME_TYPE)
        do {
            val id = cursor.getString(idIndex)
            val title = cursor.getString(titleIndex)
            val display = cursor.getString(displayIndex)
            val size = FileUtils.convertFileSize(cursor.getLong(sizeIndex), true)
            listFile.add(PhotoItem(id, display, id,
                    Uri.parse(MediaStore.Images.Media.EXTERNAL_CONTENT_URI.toString() + "/" + id), size))
        } while (cursor.moveToNext())
        _liveData.postValue(Resource.success(listFile as K))
    }

    private fun postFileData(dir: File) {
        val listFile: MutableList<FileItem> = mutableListOf()
        val files = dir.listFiles()

        if (files != null) {
            for (item in files) {
                if (item.isDirectory) {
//                    postFileData(item)
                } else {
                    listFile.add(FileItem(item.path, item.name, item.path, item.length().toString()))
                }
            }
            _liveData.postValue(Resource.success(listFile as K))
        }
    }

    private fun queriesAudio(contentResolver: ContentResolver): Cursor {
        val selection = MediaStore.Audio.Media.IS_MUSIC + " != 0"
        val projection = arrayOf(
                MediaStore.Audio.Media._ID,
                MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.SIZE)
        val cursor = contentResolver.query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                projection,
                selection,
                null,
                MediaStore.Audio.Media.TITLE)
        return cursor
    }

    private fun queriesVideo(contentResolver: ContentResolver): Cursor {
        val projection = arrayOf(
                MediaStore.Video.Media._ID,
                MediaStore.Video.Media.TITLE,
                MediaStore.Video.Media.DISPLAY_NAME,
                MediaStore.Video.Media.BUCKET_DISPLAY_NAME,
                MediaStore.Video.Media.DURATION,
                MediaStore.Video.Media.DATE_MODIFIED,
                MediaStore.Video.Media.SIZE,
                MediaStore.Video.Media.MIME_TYPE)
        val orderBy = MediaStore.Video.Media.DEFAULT_SORT_ORDER
        val cursor = contentResolver.query(MediaStore.Video.Media.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                orderBy)
        return cursor
    }

    private fun queriesPhoto(contentResolver: ContentResolver): Cursor {
        val projection = arrayOf(
                MediaStore.Images.Media._ID,
                MediaStore.Images.Media.TITLE,
                MediaStore.Images.Media.DISPLAY_NAME,
                MediaStore.Images.Media.BUCKET_DISPLAY_NAME,
                MediaStore.Images.Media.DATA,
                MediaStore.Images.Media.DATE_MODIFIED,
                MediaStore.Images.Media.SIZE,
                MediaStore.Images.Media.MIME_TYPE)
        val orderBy = MediaStore.Images.Media.DEFAULT_SORT_ORDER
        val cursor = contentResolver.query(MediaStore.Images.Media.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                orderBy)
        return cursor
    }
}
package com.huypt.filetransfer.service

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.content.pm.ApplicationInfo
import android.content.pm.PackageManager
import android.util.Log
import com.huypt.filetransfer.data.AppItem
import com.huypt.filetransfer.data.Resource
import com.huypt.filetransfer.util.FileUtils
import com.huypt.filetransfer.util.Utils
import java.io.File


class FetchListAppTask constructor(private val packageManager: PackageManager) : Runnable {

    private val _liveData = MutableLiveData<Resource<List<AppItem>>>()
    val liveData: LiveData<Resource<List<AppItem>>> = _liveData

    init {
        _liveData.postValue(Resource.loading(null))
    }

    override fun run() {
        val listApplication: List<ApplicationInfo> = packageManager.getInstalledApplications(
                PackageManager.GET_META_DATA)
        val listCustomApp: MutableList<AppItem> = mutableListOf()
        for (ai: ApplicationInfo in listApplication) {
            Log.d("home fragment", ai.packageName)
            Log.d("home fragment", ai.sourceDir)
            if (Utils.isUserApp(ai)) {
                val icon = packageManager.getApplicationIcon(ai)
                val name = packageManager.getApplicationLabel(ai)
                val file = File(ai.sourceDir)
                val size = FileUtils.convertFileSize(file.length(), true)
                val item = AppItem(ai.uid.toString(), name.toString(), icon, ai.sourceDir, size)
                listCustomApp.add(item)
            }
        }
        _liveData.postValue(Resource.success(listCustomApp))
    }
}
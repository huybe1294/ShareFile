package com.huypt.filetransfer.ui.file

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.ContentResolver
import com.huypt.filetransfer.data.FileItem
import com.huypt.filetransfer.data.Resource
import com.huypt.filetransfer.repository.AppRepository

class FileViewModel(appRepository: AppRepository,
                    contentResolver: ContentResolver) : ViewModel() {
    var data: LiveData<Resource<List<FileItem>>>? = appRepository.getListFile(contentResolver)
}
package com.huypt.filetransfer.ui.search

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.support.v7.widget.SearchView
import android.view.View
import com.huypt.filetransfer.AppExecutors
import com.huypt.filetransfer.R
import com.huypt.filetransfer.data.FileItem
import com.huypt.filetransfer.databinding.FragmentSearchBinding
import com.huypt.filetransfer.repository.AppRepository
import com.huypt.filetransfer.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_search.*
import org.jetbrains.anko.error
import org.jetbrains.anko.support.v4.toast

class SearchFragment : BaseFragment<FragmentSearchBinding, SearchViewModel>() {
    private lateinit var searchAdapter: SearchAdapter
    private lateinit var appExecutors: AppExecutors
    private var listData: List<FileItem>? = null

    companion object {
        fun newInstance(): SearchFragment {
            return SearchFragment()
        }
    }

    override fun bindLayout() = R.layout.fragment_search

    override fun initData(bundle: Bundle?) {
        appExecutors = AppExecutors.instance
        searchAdapter = SearchAdapter(appExecutors) { item ->
            toast(item.title)
        }
    }

    override fun initView() {
        imgBack.setOnClickListener(this)
        mBinding.recyclerView.adapter = searchAdapter

        mViewModel.data.observe(this, Observer { repo ->
            if (repo != null) {
                searchAdapter.submitList(repo.data)
            } else {
                searchAdapter.submitList(emptyList())
            }
        })

    }

    override fun getViewModel() = SearchViewModel(AppRepository.instance, context!!.contentResolver)

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.imgBack -> {
                baseActivity?.onBackPressed()
            }
        }
    }
}




//                mBinding.searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
//                    override fun onQueryTextSubmit(query: String?): Boolean {
//                        return false
//                    }
//
//                    override fun onQueryTextChange(newText: String?): Boolean {
//                        if (repo.data != null)
//                            for (item in repo.data) {
//                                if (newText?.equals(item.title)!!) {
//
//                                }
//                            }
//                        return false
//                    }
//
//                })
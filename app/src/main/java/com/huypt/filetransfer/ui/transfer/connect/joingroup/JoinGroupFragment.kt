package com.huypt.filetransfer.ui.transfer.connect.joingroup

import android.os.Bundle
import android.view.View
import com.huypt.filetransfer.R
import com.huypt.filetransfer.databinding.FragmentJoinGroupBinding
import com.huypt.filetransfer.ui.base.BaseFragment

class JoinGroupFragment : BaseFragment<FragmentJoinGroupBinding, JoinGroupViewModel>() {
    override fun bindLayout() = R.layout.fragment_join_group

    override fun initData(bundle: Bundle?) {
    }

    override fun initView() {
    }

    override fun getViewModel() = JoinGroupViewModel()

    override fun onClick(v: View?) {
    }
}
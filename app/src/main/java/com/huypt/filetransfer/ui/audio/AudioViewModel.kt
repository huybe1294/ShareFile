package com.huypt.filetransfer.ui.audio

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.ContentResolver
import com.huypt.filetransfer.data.AudioItem
import com.huypt.filetransfer.data.Resource
import com.huypt.filetransfer.repository.AppRepository

class AudioViewModel(appRepository: AppRepository,
                     contentResolver: ContentResolver) : ViewModel() {
    var data: LiveData<Resource<List<AudioItem>>>? = appRepository.getListAudioFile(contentResolver)
}
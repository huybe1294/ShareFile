package com.huypt.filetransfer.ui.transfer.connect

import android.os.Bundle
import android.view.View
import com.huypt.filetransfer.R
import com.huypt.filetransfer.databinding.FragmentConnectBinding
import com.huypt.filetransfer.ui.base.BaseFragment

class ConnectFragment : BaseFragment<FragmentConnectBinding, ConnectViewModel>() {

    companion object {
        fun newInstance(): ConnectFragment {
            return ConnectFragment()
        }
    }

    override fun bindLayout() = R.layout.fragment_connect

    override fun initData(bundle: Bundle?) {
    }

    override fun initView() {
    }

    override fun getViewModel() = ConnectViewModel()

    override fun onClick(v: View?) {
    }
}
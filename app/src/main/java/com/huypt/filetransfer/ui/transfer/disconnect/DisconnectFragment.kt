package com.huypt.filetransfer.ui.transfer.disconnect

import android.os.Bundle
import android.view.View
import com.huypt.filetransfer.R
import com.huypt.filetransfer.databinding.FragmentDisconnectBinding
import com.huypt.filetransfer.ui.base.BaseFragment

class DisconnectFragment : BaseFragment<FragmentDisconnectBinding, DisconnectViewModel>() {
    override fun bindLayout() = R.layout.fragment_connect

    override fun initData(bundle: Bundle?) {
    }

    override fun initView() {
    }

    override fun getViewModel() = DisconnectViewModel()

    override fun onClick(v: View?) {
    }
}
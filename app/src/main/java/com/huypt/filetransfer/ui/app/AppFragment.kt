package com.huypt.filetransfer.ui.app

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.huypt.filetransfer.AppExecutors
import com.huypt.filetransfer.R
import com.huypt.filetransfer.databinding.FragmentAppBinding
import com.huypt.filetransfer.repository.AppRepository
import com.huypt.filetransfer.ui.base.BaseFragment
import org.jetbrains.anko.support.v4.toast

class AppFragment : BaseFragment<FragmentAppBinding, AppViewModel>() {
    private lateinit var appAdapter: AppAdapter
    private lateinit var appExecutors: AppExecutors

    companion object {
        fun newInstance(): AppFragment {
            return AppFragment()
        }
    }

    override fun bindLayout() = R.layout.fragment_app

    override fun initData(bundle: Bundle?) {
        appExecutors = AppExecutors.instance
        appAdapter = AppAdapter(appExecutors) { item ->
            toast(item.title)
        }
    }

    override fun initView() {
        mBinding.recyclerView.adapter = appAdapter


        mViewModel.data?.observe(this, Observer { repo ->
            mBinding.resource =repo
            if (repo != null) {
                appAdapter.submitList(repo.data)
            } else {
                appAdapter.submitList(emptyList())
            }
        })
    }

    override fun getViewModel() = AppViewModel(AppRepository.instance, context!!.packageManager)

    override fun onClick(v: View?) {
    }
}
package com.huypt.filetransfer.ui.base

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProviders
import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.IdRes
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import android.view.View
import com.huypt.filetransfer.util.FragmentUtil
import org.jetbrains.anko.AnkoLogger

abstract class BaseActivity<B : ViewDataBinding, V : ViewModel> :
        AppCompatActivity(),
        View.OnClickListener,
        AnkoLogger {

    @IdRes
    open fun getContainerId(): Int = 0

    @LayoutRes
    protected abstract fun bindLayout(): Int

    protected abstract fun initData(bundle: Bundle?)

    protected abstract fun initView()

    protected abstract fun getViewModelClass(): Class<V>

    protected lateinit var mBinding: B
    protected lateinit var mViewModel: V

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val bundle = intent.extras
        initData(bundle)
        mBinding = DataBindingUtil.setContentView(this, bindLayout())
        mViewModel = ViewModelProviders.of(this).get(getViewModelClass())
        initFragmentController()
        initView()
    }

    override fun onBackPressed() {
        val fragment = FragmentUtil.getCurrentFragment(this)
        if (fragment is BaseFragment<*, *> && fragment.onBackPressed()) {
            // TODO: 9/7/2018
        } else {
            super.onBackPressed()
        }
    }

    /*====================[START] FRAGMENT CONTROLLER ==========================*/
    private var fragmentController: FragmentController? = null

    private fun initFragmentController() {
        if (getContainerId() != 0) {
            fragmentController = FragmentController(supportFragmentManager, getContainerId())
        } else {
        }
    }

    @Throws(NullPointerException::class)
    fun switchScreenOnContainer(baseFragment: BaseFragment<*, *>, option: FragmentController.Option) {
        val fragmentController = fragmentController
        fragmentController?.switchFragmentWithInstance(baseFragment, option)
                ?: throw NullPointerException("Fragment controller null")
    }

    @Throws(NullPointerException::class)
    fun switchScreenOnContainer(baseFragment: BaseFragment<*, *>?) {
        if (baseFragment != null) {
            val option = FragmentController.Option.Builder()
                    .setTag(baseFragment.javaClass.simpleName)
                    .option
            switchScreenOnContainer(baseFragment, option)
        } else {
            throw NullPointerException("Instance fragment is null")
        }
    }
    /*========================[END] FRAGMENT CONTROLLER ==========================================*/
}

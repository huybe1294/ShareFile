package com.huypt.filetransfer.ui.main

import android.Manifest
import android.arch.lifecycle.ViewModel
import android.content.DialogInterface
import android.os.Bundle
import android.view.View
import com.github.florent37.runtimepermission.kotlin.askPermission
import com.huypt.filetransfer.R
import com.huypt.filetransfer.databinding.ActivityMainBinding
import com.huypt.filetransfer.ui.base.BaseActivity
import com.huypt.filetransfer.util.DialogUtils
import info.whitebyte.hotspotmanager.NsdHelper

class MainActivity : BaseActivity<ActivityMainBinding, MainViewModel>() {
    private var isRunningRequestPermission: Boolean = false
    private var nsdHelper: NsdHelper? = null

    override fun bindLayout() = R.layout.activity_main

    override fun getContainerId() = R.id.main_container

    override fun initData(bundle: Bundle?) {
        nsdHelper = NsdHelper(this)
    }

    override fun initView() {}

    override fun getViewModelClass() = MainViewModel::class.java

    override fun onClick(v: View?) {}

    private fun requestPermission() {
        isRunningRequestPermission = true
        askPermission(
                Manifest.permission.WRITE_EXTERNAL_STORAGE) {
            if (it.isAccepted) {
                switchScreenOnContainer(MainFragment.newInstance())
            } else if (it.hasForeverDenied()) {
                DialogUtils.showActionDialog(this, getString(R.string.dialog_require_permission_title),
                        getString(R.string.dialog_require_permisstion_message),
                        getString(R.string.dialog_require_permission_go_to_setting),
                        DialogInterface.OnClickListener { _, _ ->
                            it.goToSettings()
                            isRunningRequestPermission = false
                        }
                        ,
                        getString(R.string.dialog_require_permission_negative),
                        DialogInterface.OnClickListener { _, _ -> finish() }, false)
            }
        }.onDeclined { e ->
            DialogUtils.showActionDialog(this, getString(R.string.dialog_require_permission_title),
                    getString(R.string.dialog_require_permisstion_message),
                    getString(R.string.dialog_require_permission_Authorization),
                    DialogInterface.OnClickListener { _, _ -> if (e.hasForeverDenied()) e.goToSettings() else requestPermission() },
                    getString(R.string.dialog_require_permission_negative),
                    DialogInterface.OnClickListener { _, _ -> finish() }, false)
        }
    }

    override fun onResume() {
        super.onResume()
        if (!isRunningRequestPermission) {
            requestPermission()
        }
        nsdHelper?.registerService(1337)
        nsdHelper?.discoverServices()
    }

    override fun onPause() {
        nsdHelper?.stopDiscovery()
        super.onPause()
    }

    override fun onDestroy() {
        nsdHelper?.tearDown()
        super.onDestroy()
    }
}

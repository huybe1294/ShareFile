package com.huypt.filetransfer.ui.app

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.pm.PackageManager
import com.huypt.filetransfer.data.AppItem
import com.huypt.filetransfer.data.Resource
import com.huypt.filetransfer.repository.AppRepository


class AppViewModel(appRepository: AppRepository,
                   packageManager: PackageManager) : ViewModel() {

    var data: LiveData<Resource<List<AppItem>>>? = appRepository.getListApplication(packageManager)
}
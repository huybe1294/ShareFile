package com.huypt.filetransfer.ui.video

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.View
import com.huypt.filetransfer.AppExecutors
import com.huypt.filetransfer.R
import com.huypt.filetransfer.databinding.FragmentVideoBinding
import com.huypt.filetransfer.repository.AppRepository
import com.huypt.filetransfer.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_video.*
import org.jetbrains.anko.support.v4.toast

class VideoFragment : BaseFragment<FragmentVideoBinding, VideoViewModel>() {

    private lateinit var videoAdapter: VideoAdapter
    private lateinit var appExecutors: AppExecutors

    companion object {
        fun newInstance(): VideoFragment {
            return VideoFragment()
        }
    }

    override fun bindLayout() = R.layout.fragment_video

    override fun initData(bundle: Bundle?) {
        appExecutors = AppExecutors.instance
        videoAdapter = VideoAdapter(appExecutors) { item ->
            toast(item.title)
        }
    }

    override fun initView() {
        mBinding.recyclerView.adapter = videoAdapter

        mViewModel.data.observe(this, Observer { repo ->
            mBinding.resource = repo
            if (repo != null) {
                videoAdapter.submitList(repo.data)
            } else {
                videoAdapter.submitList(emptyList())
            }
        })
    }

    override fun getViewModel() = VideoViewModel(AppRepository.instance, context!!.contentResolver)

    override fun onClick(v: View?) {
    }
}
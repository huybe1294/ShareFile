package com.huypt.filetransfer.ui.audio

import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.huypt.filetransfer.AppExecutors
import com.huypt.filetransfer.R
import com.huypt.filetransfer.data.AudioItem
import com.huypt.filetransfer.databinding.ItemAudioBinding
import com.huypt.filetransfer.ui.base.DataBoundListAdapter

class AudioAdapter(appExecutor: AppExecutors, private val callBack: ((AudioItem) -> Unit))
    : DataBoundListAdapter<AudioItem, ItemAudioBinding>(appExecutor,
        diffCallback = object : DiffUtil.ItemCallback<AudioItem>() {
            override fun areItemsTheSame(oldItem: AudioItem, newItem: AudioItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: AudioItem, newItem: AudioItem): Boolean {
                return oldItem.id == newItem.id && oldItem.uri == newItem.uri
            }

        }) {

    override fun createBinding(parent: ViewGroup): ItemAudioBinding {
        val binding = DataBindingUtil.inflate<ItemAudioBinding>(LayoutInflater.from(parent.context),
                R.layout.item_audio,
                parent,
                false)
        binding.root.setOnClickListener { _ -> binding.item?.let { callBack } }
        return binding
    }

    override fun bind(binding: ItemAudioBinding, item: AudioItem) {
        binding.item = item
    }
}
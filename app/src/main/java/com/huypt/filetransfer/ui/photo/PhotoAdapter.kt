package com.huypt.filetransfer.ui.photo

import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.huypt.filetransfer.AppExecutors
import com.huypt.filetransfer.R
import com.huypt.filetransfer.data.PhotoItem
import com.huypt.filetransfer.databinding.ItemPhotoBinding
import com.huypt.filetransfer.ui.base.DataBoundListAdapter

class PhotoAdapter(appExecutor: AppExecutors, private val callBack: ((PhotoItem) -> Unit))
    : DataBoundListAdapter<PhotoItem, ItemPhotoBinding>(appExecutor,
        diffCallback = object : DiffUtil.ItemCallback<PhotoItem>() {
            override fun areItemsTheSame(oldItem: PhotoItem, newItem: PhotoItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: PhotoItem, newItem: PhotoItem): Boolean {
                return oldItem.id == newItem.id && oldItem.uri == newItem.uri
            }

        }) {

    override fun createBinding(parent: ViewGroup): ItemPhotoBinding {
        val binding = DataBindingUtil.inflate<ItemPhotoBinding>(LayoutInflater.from(parent.context),
                R.layout.item_photo,
                parent,
                false)
        binding.root.setOnClickListener { _ -> binding.item?.let { callBack } }
        return binding
    }

    override fun bind(binding: ItemPhotoBinding, item: PhotoItem) {
        binding.item = item
        Glide.with(binding.root.context).load(item.thumb).into(binding.imgPhoto)
    }
}
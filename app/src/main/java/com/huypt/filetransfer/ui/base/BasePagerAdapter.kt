package com.huypt.filetransfer.ui.base

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter

class BasePagerAdapter(fragmentManager: FragmentManager,
    private val itemFragmentList: List<ItemFragment>) : FragmentStatePagerAdapter(fragmentManager) {

  override fun getItem(position: Int): Fragment {
    return itemFragmentList[position].fragment
  }

  override fun getCount(): Int {
    return itemFragmentList.size
  }

  override fun getPageTitle(position: Int): CharSequence? {
    return itemFragmentList[position].title
  }
}
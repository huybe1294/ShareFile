package com.huypt.filetransfer.ui.search

import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import com.huypt.filetransfer.AppExecutors
import com.huypt.filetransfer.R
import com.huypt.filetransfer.data.FileItem
import com.huypt.filetransfer.databinding.ItemFileBinding
import com.huypt.filetransfer.ui.base.DataBoundListAdapter

class SearchAdapter(appExecutor: AppExecutors, private val callBack: ((FileItem) -> Unit))
    : DataBoundListAdapter<FileItem, ItemFileBinding>(appExecutor,
        diffCallback = object : DiffUtil.ItemCallback<FileItem>() {
            override fun areItemsTheSame(oldItem: FileItem, newItem: FileItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: FileItem, newItem: FileItem): Boolean {
                return oldItem.id == newItem.id && oldItem.uri == newItem.uri
            }
        }) {

    override fun createBinding(parent: ViewGroup): ItemFileBinding {
        val binding = DataBindingUtil.inflate<ItemFileBinding>(
                LayoutInflater.from(parent.context),
                R.layout.item_file,
                parent,
                false)
        binding.root.setOnClickListener { _ -> binding.item?.let { callBack } }
        return binding
    }

    override fun bind(binding: ItemFileBinding, item: FileItem) {
        binding.item = item
    }
}
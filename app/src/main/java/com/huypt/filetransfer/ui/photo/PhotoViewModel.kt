package com.huypt.filetransfer.ui.photo

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.ContentResolver
import com.huypt.filetransfer.data.PhotoItem
import com.huypt.filetransfer.data.Resource
import com.huypt.filetransfer.repository.AppRepository

class PhotoViewModel (appRepository: AppRepository,
                      contentResolver: ContentResolver) : ViewModel() {
    val data: LiveData<Resource<List<PhotoItem>>> = appRepository.getListPhotoFile(contentResolver)
}
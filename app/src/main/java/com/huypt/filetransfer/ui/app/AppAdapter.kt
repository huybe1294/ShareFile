package com.huypt.filetransfer.ui.app

import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import com.huypt.filetransfer.AppExecutors
import com.huypt.filetransfer.R
import com.huypt.filetransfer.data.AppItem
import com.huypt.filetransfer.databinding.ItemAppBinding
import com.huypt.filetransfer.ui.base.DataBoundListAdapter

class AppAdapter(
        appExecutors: AppExecutors,
        private val callback: ((AppItem) -> Unit)?) : DataBoundListAdapter<AppItem, ItemAppBinding>(
        appExecutors = appExecutors,
        diffCallback = object : DiffUtil.ItemCallback<AppItem>() {
            override fun areItemsTheSame(oldItem: AppItem, newItem: AppItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: AppItem, newItem: AppItem): Boolean {
                return oldItem.id == newItem.id && oldItem.uri == newItem.uri
            }
        }) {

    override fun createBinding(parent: ViewGroup): ItemAppBinding {
        val binding = DataBindingUtil.inflate<ItemAppBinding>(LayoutInflater.from(parent.context),
                R.layout.item_app, parent, false)
        binding.root.setOnClickListener { _ -> binding.item?.let { callback?.invoke(it) } }
        return binding
    }

    override fun bind(binding: ItemAppBinding, item: AppItem) {
        binding.item = item
    }
}
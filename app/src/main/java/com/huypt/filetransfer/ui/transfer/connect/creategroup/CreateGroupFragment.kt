package com.huypt.filetransfer.ui.transfer.connect.creategroup

import android.os.Bundle
import android.view.View
import com.huypt.filetransfer.R
import com.huypt.filetransfer.databinding.FragmentCreateGroupBinding
import com.huypt.filetransfer.ui.base.BaseFragment

class CreateGroupFragment : BaseFragment<FragmentCreateGroupBinding, CreateGroupViewModel>() {
    override fun bindLayout() = R.layout.fragment_create_group
    override fun initData(bundle: Bundle?) {
    }

    override fun initView() {
    }

    override fun getViewModel() = CreateGroupViewModel()

    override fun onClick(v: View?) {
    }
}
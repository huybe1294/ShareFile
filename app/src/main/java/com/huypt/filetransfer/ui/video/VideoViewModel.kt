package com.huypt.filetransfer.ui.video

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.ViewModel
import android.content.ContentResolver
import com.huypt.filetransfer.data.VideoItem
import com.huypt.filetransfer.repository.AppRepository
import com.huypt.filetransfer.data.Resource

class VideoViewModel (appRepository: AppRepository,
                      contentResolver: ContentResolver) : ViewModel() {
    var data: LiveData<Resource<List<VideoItem>>>  = appRepository.getListVideoFile(contentResolver)
}

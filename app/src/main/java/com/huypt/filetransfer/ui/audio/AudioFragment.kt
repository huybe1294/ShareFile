package com.huypt.filetransfer.ui.audio

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.View
import com.huypt.filetransfer.AppExecutors
import com.huypt.filetransfer.R
import com.huypt.filetransfer.databinding.FragmentAudioBinding
import com.huypt.filetransfer.repository.AppRepository
import com.huypt.filetransfer.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_audio.*
import org.jetbrains.anko.support.v4.toast

class AudioFragment : BaseFragment<FragmentAudioBinding, AudioViewModel>() {
    private lateinit var audioAdapter: AudioAdapter
    private lateinit var appExecutors: AppExecutors

    companion object {
        fun newInstance(): AudioFragment {
            return AudioFragment()
        }
    }

    override fun bindLayout() = R.layout.fragment_audio

    override fun initData(bundle: Bundle?) {
        appExecutors = AppExecutors.instance
        audioAdapter = AudioAdapter(appExecutors) { item ->
            toast(item.title)
        }
    }

    override fun initView() {
        recyclerView.adapter = audioAdapter

        mViewModel.data?.observe(this, Observer { repo ->
            mBinding.resource =repo
            if (repo != null) {
                audioAdapter.submitList(repo.data)
            } else {
                audioAdapter.submitList(emptyList())
            }
        })
    }

    override fun getViewModel() = AudioViewModel(AppRepository.instance, context!!.contentResolver)

    override fun onClick(v: View?) {
    }
}
package com.huypt.filetransfer.ui.photo

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.View
import com.huypt.filetransfer.AppExecutors
import com.huypt.filetransfer.R
import com.huypt.filetransfer.databinding.FragmentPhotoBinding
import com.huypt.filetransfer.repository.AppRepository
import com.huypt.filetransfer.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_photo.*
import org.jetbrains.anko.support.v4.toast

class PhotoFragment : BaseFragment<FragmentPhotoBinding, PhotoViewModel>() {
    private lateinit var photoAdapter: PhotoAdapter
    private lateinit var appExecutors: AppExecutors

    companion object {
        fun newInstance(): PhotoFragment {
            return PhotoFragment()
        }
    }

    override fun bindLayout() = R.layout.fragment_photo

    override fun initData(bundle: Bundle?) {
        appExecutors = AppExecutors.instance
        photoAdapter = PhotoAdapter(appExecutors) {item->
            toast(item.title)
        }
    }

    override fun initView() {
        recyclerView.adapter = photoAdapter

        mViewModel.data.observe(this, Observer { repo ->
            mBinding.resource =repo
            if (repo != null) {
                photoAdapter.submitList(repo.data)
            } else {
                photoAdapter.submitList(emptyList())
            }
        })
    }

    override fun getViewModel() = PhotoViewModel(AppRepository.instance, context!!.contentResolver)
    override fun onClick(v: View?) {
    }
}
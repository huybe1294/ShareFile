package com.huypt.filetransfer.ui.main

import android.arch.lifecycle.Observer
import android.content.res.ColorStateList
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.view.View
import com.bumptech.glide.Glide
import com.huypt.filetransfer.R
import com.huypt.filetransfer.databinding.FragmentMainBinding
import com.huypt.filetransfer.repository.AppRepository
import com.huypt.filetransfer.ui.app.AppFragment
import com.huypt.filetransfer.ui.audio.AudioFragment
import com.huypt.filetransfer.ui.base.BaseFragment
import com.huypt.filetransfer.ui.base.BasePagerAdapter
import com.huypt.filetransfer.ui.base.ItemFragment
import com.huypt.filetransfer.ui.file.FileFragment
import com.huypt.filetransfer.ui.photo.PhotoFragment
import com.huypt.filetransfer.ui.search.SearchFragment
import com.huypt.filetransfer.ui.transfer.connect.ConnectFragment
import com.huypt.filetransfer.ui.video.VideoFragment
import kotlinx.android.synthetic.main.fragment_main.*

class MainFragment : BaseFragment<FragmentMainBinding, MainFragmentViewModel>() {

    companion object {
        fun newInstance(): MainFragment {
            return MainFragment()
        }
    }

    override fun bindLayout() = R.layout.fragment_main

    override fun initData(bundle: Bundle?) {
    }

    override fun initView() {
        setupViewPager()
        tabLayout.setupWithViewPager(viewPager)

        fab.setOnClickListener(this)
        imgSearch.setOnClickListener(this)

        tabLayout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {}

            override fun onTabUnselected(tab: TabLayout.Tab?) {}

            override fun onTabSelected(tab: TabLayout.Tab?) {
                when (tab?.position) {
                    0 -> {
                        updateUI(R.color.colorPrimaryDark, R.color.colorPrimary, R.color.colorPrimary, R.drawable.ic_slow_motion_video_white_24dp)
                    }
                    1 -> {
                        updateUI(R.color.orange_900_dark, R.color.orange_900, R.color.orange_900, R.drawable.ic_audio_album_white_24dp)
                    }
                    2 -> {
                        updateUI(R.color.green_900_dark, R.color.green_900, R.color.green_900, R.drawable.ic_app_white_24dp)
                    }
                    3 -> {
                        updateUI(R.color.red_900_dark, R.color.red_900, R.color.red_900, R.drawable.ic_photo_album_white_24dp)
                    }
                    4 -> {
                        updateUI(R.color.cyan_900_dark, R.color.cyan_900, R.color.cyan_900, R.drawable.ic__file_white_24dp)
                    }
                }
            }
        })
    }

    override fun getViewModel() =MainFragmentViewModel()

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.fab -> {
                baseActivity?.switchScreenOnContainer(ConnectFragment.newInstance())
            }

            R.id.imgSearch -> {
                baseActivity?.switchScreenOnContainer(SearchFragment.newInstance())
            }
        }
    }

    private fun setupViewPager() {
        val itemFragmentList = mutableListOf<ItemFragment>()
        itemFragmentList.add(ItemFragment(getString(R.string.video_fragment_title), VideoFragment.newInstance()))
        itemFragmentList.add(ItemFragment(getString(R.string.audio_fragment_title), AudioFragment.newInstance()))
        itemFragmentList.add(ItemFragment(getString(R.string.app_fragment_title), AppFragment.newInstance()))
        itemFragmentList.add(ItemFragment(getString(R.string.photo_fragment_title), PhotoFragment.newInstance()))
        itemFragmentList.add(ItemFragment(getString(R.string.file_fragment_title), SearchFragment.newInstance()))

        viewPager.adapter = BasePagerAdapter(childFragmentManager, itemFragmentList)
        viewPager.offscreenPageLimit = 4
    }

    private fun updateUI(statusBar: Int, fabColor: Int, toolBarColor: Int, imgLogoSrc: Int) {
        activity?.window?.statusBarColor = resources.getColor(statusBar)
        fab.backgroundTintList = ColorStateList.valueOf(resources.getColor(fabColor))
        toolbar.setBackgroundColor(resources.getColor(toolBarColor))
        Glide.with(activity!!).load(imgLogoSrc).into(imgLogo)
    }
}
package com.huypt.filetransfer.ui.video

import android.databinding.DataBindingUtil
import android.support.v7.util.DiffUtil
import android.view.LayoutInflater
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.huypt.filetransfer.AppExecutors
import com.huypt.filetransfer.R
import com.huypt.filetransfer.data.VideoItem
import com.huypt.filetransfer.databinding.ItemVideoBinding
import com.huypt.filetransfer.ui.base.DataBoundListAdapter

class VideoAdapter(appExecutor: AppExecutors, private val callBack: ((VideoItem) -> Unit))
    : DataBoundListAdapter<VideoItem, ItemVideoBinding>(appExecutor,
        diffCallback = object : DiffUtil.ItemCallback<VideoItem>() {
            override fun areItemsTheSame(oldItem: VideoItem, newItem: VideoItem): Boolean {
                return oldItem.id == newItem.id
            }

            override fun areContentsTheSame(oldItem: VideoItem, newItem: VideoItem): Boolean {
                return oldItem.id == newItem.id && oldItem.uri == newItem.uri
            }

        }) {

    override fun createBinding(parent: ViewGroup): ItemVideoBinding {
        val binding = DataBindingUtil.inflate<ItemVideoBinding>(LayoutInflater.from(parent.context),
                R.layout.item_video,
                parent,
                false)
        binding.root.setOnClickListener { _ -> binding.item?.let { callBack } }
        return binding
    }

    override fun bind(binding: ItemVideoBinding, item: VideoItem) {
        binding.item = item
        Glide.with(binding.root.context).load(item.thumb).into(binding.imgIcon)
    }
}
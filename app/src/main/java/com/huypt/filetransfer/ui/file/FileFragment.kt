package com.huypt.filetransfer.ui.file

import android.arch.lifecycle.Observer
import android.os.Bundle
import android.view.View
import com.huypt.filetransfer.AppExecutors
import com.huypt.filetransfer.R
import com.huypt.filetransfer.databinding.FragmentFileBinding
import com.huypt.filetransfer.repository.AppRepository
import com.huypt.filetransfer.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_file.*
import org.jetbrains.anko.support.v4.toast

class FileFragment : BaseFragment<FragmentFileBinding, FileViewModel>() {
    private lateinit var fileAdapter: FileAdapter
    private lateinit var appExecutors: AppExecutors

    companion object {
        fun newInstance(): FileFragment {
            return FileFragment()
        }
    }

    override fun bindLayout() = R.layout.fragment_file

    override fun initData(bundle: Bundle?) {
        appExecutors = AppExecutors.instance
        fileAdapter = FileAdapter(appExecutors) { item ->
            toast(item.title)
        }
    }

    override fun initView() {
        recyclerView.adapter = fileAdapter

        mViewModel.data?.observe(this, Observer { repo ->
            if (repo != null) {
                fileAdapter.submitList(repo.data)
            } else {
                fileAdapter.submitList(emptyList())
            }
        })
    }

    override fun getViewModel() = FileViewModel(AppRepository.instance, context!!.contentResolver)

    override fun onClick(v: View?) {
    }
}
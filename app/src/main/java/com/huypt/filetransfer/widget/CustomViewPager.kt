package com.huypt.filetransfer.widget

import android.annotation.SuppressLint
import android.content.Context
import android.content.res.TypedArray
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent
import com.huypt.filetransfer.R

class CustomViewPager : ViewPager {

    private var isDisableSmooth = false
    private var isDisableSwipe = false

    constructor(context: Context) : super(context)

    @SuppressLint("Recycle")
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs) {
        val a: TypedArray = context.obtainStyledAttributes(attrs, R.styleable.CustomViewPager)
        isDisableSmooth = a.getBoolean(R.styleable.CustomViewPager_is_disable_smooth, false)
        isDisableSwipe = a.getBoolean(R.styleable.CustomViewPager_is_disable_swipe, false)
    }

    override fun setCurrentItem(item: Int) {
        super.setCurrentItem(item, !isDisableSmooth)
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (!this.isDisableSwipe) {
            super.onTouchEvent(event)
        } else false

    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return if (!this.isDisableSwipe) {
            super.onInterceptTouchEvent(event)
        } else false

    }
}
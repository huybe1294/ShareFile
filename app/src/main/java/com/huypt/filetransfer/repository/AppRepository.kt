package com.huypt.filetransfer.repository

import android.arch.lifecycle.LiveData
import android.content.ContentResolver
import android.content.pm.PackageManager
import com.huypt.filetransfer.AppExecutors
import com.huypt.filetransfer.data.*
import com.huypt.filetransfer.service.FetchListAppTask
import com.huypt.filetransfer.service.FetchListFileTask
import com.huypt.filetransfer.data.Resource

class AppRepository private constructor() {

    companion object {
        val instance = AppRepository()
    }

    fun getListApplication(packageManager: PackageManager): LiveData<Resource<List<AppItem>>> {
        val fetchListAppTask = FetchListAppTask(packageManager)
        AppExecutors.instance.diskIO().execute(fetchListAppTask)
        return fetchListAppTask.liveData
    }

    fun getListAudioFile(contentResolver: ContentResolver): LiveData<Resource<List<AudioItem>>> {
        val fetchListFile = FetchListFileTask<List<AudioItem>>(contentResolver, TransferItem.TYPE_AUDIO)
        AppExecutors.instance.diskIO().execute(fetchListFile)
        return fetchListFile.liveData
    }

    fun getListVideoFile(contentResolver: ContentResolver): LiveData<Resource<List<VideoItem>>> {
        val fetchListFile = FetchListFileTask<List<VideoItem>>(contentResolver, TransferItem.TYPE_VIDEO)
        AppExecutors.instance.diskIO().execute(fetchListFile)
        return fetchListFile.liveData
    }

    fun getListPhotoFile(contentResolver: ContentResolver): LiveData<Resource<List<PhotoItem>>> {
        val fetchListFile = FetchListFileTask<List<PhotoItem>>(contentResolver, TransferItem.TYPE_PHOTO)
        AppExecutors.instance.diskIO().execute(fetchListFile)
        return fetchListFile.liveData
    }

    fun getListFile(contentResolver: ContentResolver): LiveData<Resource<List<FileItem>>> {
        val fetchListFile = FetchListFileTask<List<FileItem>>(contentResolver, TransferItem.TYPE_FILE)
        AppExecutors.instance.diskIO().execute(fetchListFile)
        return fetchListFile.liveData
    }
}